import React from "react";
import PerfectScrollbar from "perfect-scrollbar";
import {Redirect, Route, RouteComponentProps, Switch} from "react-router-dom";

import {MyNavbar} from "components/nav/Navbar";
import Footer from "components/footer/Footer";
import {Sidebar} from "components/sidebar/Sidebar";

import {flatRoutes, Route as MyRoute} from "common/routes";
import {PrivateRoute} from "../common/PrivateRoute";
import {NotLoggedIn} from "../pages/NotLoggedIn";
import {NotFound} from "../pages/NotFound";
import {AppHandle} from "../common/LoginHandler";
import {MyReducerState} from "../common/reducers";
import {connect} from "react-redux";

type MainLayoutState = {
    backgroundColor: string
    activeColor: string
    isLoggedIn: boolean
}

type MainLayoutPropsFromRedux = {
    routes: MyRoute[]
}

class MainLayoutView extends React.Component<RouteComponentProps & MainLayoutPropsFromRedux, MainLayoutState> {
    private readonly mainPanel: React.RefObject<HTMLDivElement>;
    private ps: PerfectScrollbar | null;

    constructor(props: RouteComponentProps & MainLayoutPropsFromRedux) {
        super(props);
        this.state = {
            backgroundColor: "black",
            activeColor: "info",
            isLoggedIn: false
        };
        this.mainPanel = React.createRef();
        this.ps = null;
    }

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            if (this.mainPanel.current) {
                this.ps = new PerfectScrollbar(this.mainPanel.current);
            }
            document.body.classList.toggle("perfect-scrollbar-on");
        }
    }

    componentWillUnmount() {
        if (navigator.platform.indexOf("Win") > -1) {
            if (this.ps) {
                this.ps.destroy();
            }
            document.body.classList.toggle("perfect-scrollbar-on");
        }
    }

    componentDidUpdate(e: RouteComponentProps) {
        if (e.history.action === "PUSH") {
            if (this.mainPanel.current) {
                this.mainPanel.current.scrollTop = 0;
            }
            if (document.scrollingElement) {
                document.scrollingElement.scrollTop = 0;
            }
        }
    }

    render() {
        return (
            <div className="wrapper">
                <Sidebar
                    {...this.props}
                    bgColor={this.state.backgroundColor}
                    activeColor={this.state.activeColor}
                />
                <div className="main-panel" ref={this.mainPanel}>
                    <MyNavbar {...this.props} />
                    <Switch>
                        <Route exact path="/apps">
                            <Redirect to={"/"}/>
                        </Route>

                        {/* Match apps first, sort of a hack */}
                        <PrivateRoute path="/apps/:appName"
                                      render={(props) =>
                                          <AppHandle appName={props.match.params.appName}/>}/>

                        {/* When given path /needs-login show the NotLoggedIn page */}
                        <Route exact path="/needs-login" component={NotLoggedIn}/>

                        {/* Render given routes */}
                        {
                            flatRoutes(this.props.routes).map((prop, key) => (
                                <PrivateRoute {...prop} key={key}/>
                            ))
                        }

                        {/* In case someone was on pending app admin and the route was removed, redirect to activate */}
                        <Route exact path="/pending">
                            <Redirect to={"/activate"}/>
                        </Route>

                        {/* None of the routes match, show not found page */}
                        <Route path="/" component={NotFound}/>
                    </Switch>
                    <Footer fluid default/>
                </div>
            </div>
        );
    }
}

const mapState = (state: MyReducerState) => ({
    routes: state.routes
});

export const MainLayout = connect(mapState)(MainLayoutView);
