export type SparqlPlaygroundProps = {
    selectable: boolean
    onClickCell?: (entityId: string) => void
    tableProps: Object
}

export type SparqlResponse = {
    head: SparqlHead,
    results: SparqlResults,
}

export interface DataCol {
    dataField: string,
    text: string,
    events?: TableEvents | undefined,
    hidden?: boolean,
}

export interface Binding {
    [key: string]: DataNode,
}

export interface DataNode {
    type: string,
    value: string,
}

export interface DataRow {
    [key: string]: any,
}