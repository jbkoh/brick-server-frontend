import * as React from "react";
import Editor from "react-simple-code-editor";
import { Button } from "reactstrap";
import * as Prism from "prismjs";
import { getSparqlQuery } from "../../common/api";
import 'prismjs/themes/prism.css';
import "prismjs/components/prism-turtle";
import "prismjs/components/prism-sparql";
import * as Types from "./SparqlPlayground.d";
import { DEBUG } from "../../common/utils";

//@ts-ignore
import BootstrapTable from 'react-bootstrap-table-next';


export const SparqlPlayground = (props: Types.SparqlPlaygroundProps) => {
    let defaultCode: string = "";
    // User code
    if (DEBUG) {
        defaultCode = `select ?s where {
  ?s a brick:Zone_Air_Temperature_Sensor.
}`
    }
    const [code, setCode] = React.useState(defaultCode);
    // Query results
    const [queryRes, setQueryRes] = React.useState<Types.SparqlResponse | undefined>(undefined);

    // Table state fields
    const [rows, setRows] = React.useState<Types.DataRow[]>([]);
    const [cols, setCols] = React.useState<Types.DataCol[]>([]);
    const [tableLoaded, setTableLoaded] = React.useState(false);

    // should only be called when queryRes contains a value
    const loadTable = () => {
        const res: Types.SparqlResponse = queryRes! as Types.SparqlResponse;

        // Don't have result can't load
        if(!res) {
            return;
        }

        const onCellClick = (e: Event, column: Types.DataCol, columnIndex: number, row: Types.DataRow, rowIndex: number) => {
            if (props.onClickCell) {
                props.onClickCell(row[column.text]);
            }
        }

        // Init columns
        const columns: Types.DataCol[] = [];
        res.head.vars.map((col: string) => {
            columns.push({
                dataField: col,
                text: col,
                events: {
                    onClick: onCellClick
                }
            });
        });

        // Init rows
        const rows: Types.DataRow[] = [];
        res.results.bindings.map((r: Types.Binding, rowKey: number) => {
            const row: Types.DataRow = {};
            columns.map((col: Types.DataCol) => {
                row[col.text] = r[col.text].value;
            });
            row['id'] = rowKey;
            rows.push(row);
        });

        // Add in column last so above doesn't look for id column in result
        columns.push({dataField: 'id', text: "ID", hidden: true});

        setCols(columns);
        setRows(rows);
        setTableLoaded(true);
    }

    const handleClick = async () => {
        // send /GET with this.state.code
        const res = await getSparqlQuery(code);
        setQueryRes(res);
    }

    React.useEffect(() => {
        loadTable();
    }, [queryRes]);

    React.useEffect(() => {
        const handleKeyDown = (event: KeyboardEvent) => {
            // CTRL Click submits
            if (event.keyCode === 13 && event.ctrlKey) {
                handleClick();
            }
        };

        document.addEventListener('keydown', handleKeyDown);

        const cleanup = () => {
            document.removeEventListener('keydown', handleKeyDown);
        }
        return cleanup;
    }, []);

    return (
        <div className="sparql-playground">
            <form>
                <div className="editor">
                    <Editor
                        value={code}
                        onValueChange={code => setCode(code)}
                        highlight={code => Prism.highlight(code, Prism.languages.sparql, 'sparql')}
                        padding={10}
                        style={{
                            fontFamily: 'inherit',
                            fontSize: 12,
                        }}
                    />
                </div>
                <Button
                    className="submit-button btn-primary"
                    onClick={handleClick}>
                    Execute
                    </Button>
            </form>
            {
                tableLoaded &&
                <div>
                    <div className="output-label">
                        Results:
                        </div>
                    <div className="sparql-output">
                        <BootstrapTable
                            bootstrap4
                            keyField="id"
                            data={rows}
                            columns={cols}
                            {...props.tableProps}
                            />
                    </div>
                </div>
            }
        </div>
    );
}

SparqlPlayground.defaultProps = {
    tableProps: {}
}