import * as sparqlAPI from "common/api/sparql-api";
import {TreeNode} from "react-ui-tree";
import {debug} from "common/utils";

type RelationshipNode = {
    type: string
    value: string
}

export type Relationship = {
    child: RelationshipNode
    parent: RelationshipNode
}

type QueryInfo = {
    queryString: string,
    queryType: string,
}

const equipQuery: QueryInfo = {
    queryString: `select DISTINCT ?child ?parent where {
    {?parent brick:isPartOf ?child.}
    UNION
    {?child brick:isPartOf ?parent.}
    UNION
    {?parent brick:feeds ?child.}
    UNION
    {?child brick:isFedBy ?parent.}
    ?parent a/rdfs:subClassOf* brick:Equipment.
    ?child a/rdfs:subClassOf* brick:Equipment.
}`,
    queryType: "Equipment"
};

const locationQuery: QueryInfo = {
    queryString: `select ?parent ?child where {
  ?parent a/rdfs:subClassOf brick:Location.
  ?child brick:isPartOf+ ?parent.
}`,
    queryType: "Location"
};


const loadData = async (queries: Array<QueryInfo>) => {
    let rshipsGroups: Array<Array<Relationship>> = [];
    try {
        for (let i = 0; i < queries.length; i++) {
            const res = await sparqlAPI.getSparqlQuery(queries[i].queryString);
            rshipsGroups.push(res["results"]["bindings"]);
        }
    } catch (err) {
        console.log("%c Failed to load metadata: %s!", "color: red", err.message)
    }
    return rshipsGroups;

};

const _buildTree = (rships: Array<Relationship>, treeType: string): TreeNode => {
    if (rships.length === 0) {
        throw new Error("Failed to fetch relationships");
    }

    // Loop through each relationship pair:
    // Find the node for child and parent or create and insert into map if doesn't exist
    // Default set node->root = true (treat every node as a root)

    // If we found it has a child, push into children and set that node to not be a root e.g
    // parent.children.push(child)
    // child->root = false
    const nodeMap: Map<String, TreeNode> = new Map();
    for (const {child, parent} of rships) {
        debug("child: %s, %s parent", JSON.stringify(child), JSON.stringify(parent));
        // Get node for child and parent
        const childNode: TreeNode = nodeMap.has(child.value) ? nodeMap.get(child.value)! :
            {
                module: child.value,
                collapsed: true,
                clickable: true,
                root: false,
                children: undefined
            };

        const parentNode: TreeNode = nodeMap.has(parent.value) ? nodeMap.get(parent.value)! :
            {
                module: parent.value,
                collapsed: true,
                clickable: true,
                root: true,
                children: []
            };

        // Node is a child, cannot be a root
        childNode.root = false;
        if (!parentNode.children) {
            parentNode.children = [];
        }
        // Make childNode child of parent
        parentNode.children.push(childNode);
        childNode.parent = parentNode;

        // Set back on map
        nodeMap.set(child.value, childNode);
        nodeMap.set(parent.value, parentNode);
    }

    const roots = Array.from(nodeMap.values()).filter(r => r.root);

    let resRoot: TreeNode = {
        module: treeType,
        collapsed: false,
        clickable: false,
        root: true,
    };

    if (roots.length > 0) {
        roots.forEach(root => {
            root.root = false;
            if (resRoot.children === undefined) {
                resRoot.children = [];
            }
            root.parent = resRoot;
            resRoot.children!.push(root);
        });
    }

    return resRoot;
};

export const buildTrees = async () => {
    const trees: Array<TreeNode> = [];
    const queries: Array<QueryInfo> = [equipQuery, locationQuery];

    try {
        const rshipsGroups: Array<Array<Relationship>> = await loadData(queries);

        for (let i = 0; i < rshipsGroups.length; i++) {
            trees.push(_buildTree(rshipsGroups[i], queries[i].queryType));
        }
    } catch (err) {
        console.log("%c Failed to parse metadata: %s!", "color: red", err.message)
    }

    return trees;
};

