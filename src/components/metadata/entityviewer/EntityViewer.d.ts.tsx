export type EntityData = {
    relationships: Array<Array<string>>,
    entity_id: string,
    name: string,
    type: string,
}

export type EntityViewerProps = {
    moduleName: string,
    data: EntityData,
    selectEntity: (entityName: string) => void,
}
