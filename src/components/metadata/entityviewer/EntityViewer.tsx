import * as React from "react";
import {EntityViewerProps} from "./EntityViewer.d.ts";

const DEFAULT_MSG = "Select a node to view its content here.";


/**
 * We are given pairs of data (field type, value)
 *
 * For one-to-one fields we just want to show - field type : value
 *
 * We can also have one-to-many fields, "relationships", so we want to show:
 *
 * Relationship         one
 *                      two
 *                      three
 */
export const EntityViewer = (props: EntityViewerProps) => {
    if (!props.data) {
        return (
            <div className="default-node">
                <span>
                    {DEFAULT_MSG}
                </span>
            </div>
        );
    }

    // Build components for normal (field, value) pairs
    const entityDiv = Array.from(Object.entries(props.data))
        // Relationship field types are one to many, handle later
        .filter(entry => entry[0] !== "relationships" && entry[1] !== null)
        .map((entry, idxEntity) => {
            let field: string = entry[0];
            // value is a string | array<string>
            let value: string = entry[1] as string;

            if (entry[0] === "type" && (entry[1] as string).split) {
                value = (entry[1] as string).split("#")[1];
            }

            // Fields of type "type" have hyperlinks to the given url
            return (
                <React.Fragment key={`Relationship Fragment : ${idxEntity}`}>
                    <div key={`${idxEntity}: Field 1-1`} className="field">
                        {field}
                    </div>
                    <div key={`${idxEntity}: Value 1-1`} className="value">
                        {field !== "type" && value}
                        {field === "type" &&
                        <a href={entry[1] as string}>{value}</a>
                        }
                    </div>
                </React.Fragment>
            )
        });

    // loops through relationship entries maps ids to relationship type
    const rshipMap: Map<string, Array<string>> = new Map();
    props.data.relationships.forEach((rship) => {
        const relationshipStr = rship[0].split("#")[1];
        if (rshipMap.has(relationshipStr)) {
            rshipMap.get(relationshipStr)!.push(rship[1]);
        } else {
            rshipMap.set(relationshipStr, [rship[1]]);
        }
    });

    // Build relationship component (field, many values)
    const relationshipsDiv = Array.from(rshipMap.keys()).map((key, idxRship) => {
        return (
            <React.Fragment key={`Relationship Fragment : ${idxRship}`}>
                <div key={`${idxRship}: Field 1-Many`} className="field">
                    {key}
                </div>
                <div key={`${idxRship}: Value 1-Many`} className="value">
                    {rshipMap.get(key)!.map((value, idxValue) => {
                        return (
                            <div key={`${idxValue}: Value List`}>
                                {value === "admin" && value}
                                {
                                    value !== "admin" &&
                                    <a key={`${idxValue} Link`} className="entity-link" onClick={() => props.selectEntity(value)}>
                                        {value}
                                    </a>
                                }
                            </div>
                        );
                    })}
                </div>
            </React.Fragment>
        );
    });

    return (
        <>
            <div className="node-title">{props.moduleName}</div>
            <div className="entityview">
                {entityDiv}
                {relationshipsDiv}
            </div>
        </>
    )
};
