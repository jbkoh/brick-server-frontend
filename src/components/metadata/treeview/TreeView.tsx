import * as React from "react";
import Tree, { TreeNode } from "react-ui-tree";

type TreeViewProps = {
    root: TreeNode,
    onClickNodeIcon: (node: TreeNode) => any
    onClickNode: (node: TreeNode) => any
    update: (node: TreeNode) => void
    selectedNode?: TreeNode
}


type TreeViewState = {
    active?: TreeNode
}

const DEFAULT_PADDING: number = 20;

class TreeView extends React.Component<TreeViewProps, TreeViewState> {
    constructor(props: TreeViewProps) {
        super(props);
        this.state = {};
    }

    getIcon = (node: TreeNode) => {
        const click = () => {
            this.setState(Object.assign({}, this.state));
            this.props.onClickNodeIcon(node);
        };

        if (node.children && !node.root) {
            if (node.collapsed) {
                return (
                    <i onClick={click} className="nc-icon nc-minimal-right" />
                );
            } else {
                return (
                    <i onClick={click} className="nc-icon nc-minimal-down" />
                );
            }
        } else {
            return (
                <span className="indent-space" />
            );
        }
    };


    renderNode = (node: TreeNode) => {
        return (
            <div className="node">
                {this.getIcon(node)}
                <div className={(node.root) ? "section-label" : (this.props.selectedNode && node === this.props.selectedNode!) ? "label-highlight" : "label"}
                    onClick={() => this.props.onClickNode(node)} >
                    {node.module}
                </div>
            </div>
        );
    };

    handleChange = (tree: TreeNode) => {
        this.props.update(tree);
    };

    render() {
        return (
            <div>
                <Tree
                    paddingLeft={DEFAULT_PADDING}
                    draggable={false}
                    tree={this.props.root}
                    onChange={this.handleChange}
                    renderNode={this.renderNode}
                />
            </div>
        );
    }
}

export default TreeView;
