import * as React from "react";

import { TreeNode } from "react-ui-tree";
import { buildTrees } from "./Metadata";
import { getEntity } from "common/api";
import TreeView from "components/metadata/treeview/TreeView";
import {EntityData} from "components/metadata/entityviewer/EntityViewer.d.ts";
import {EntityViewer} from "components/metadata/entityviewer/EntityViewer";

type MetadataViewState = {
    root?: TreeNode,
    trees?: Array<TreeNode>,
    nodeData?: EntityData,
    selectedNode?: TreeNode,
}

class MetadataView extends React.Component<{}, MetadataViewState> {
    constructor(props: {}) {
        super(props);

        this.state = {};
    }

    onSelectIcon = (node: TreeNode) => {
        node.collapsed = !node.collapsed;
    }

    onSelectLabel = async (node: TreeNode) => {
        if (!node.clickable) { return; }
        const resp = await getEntity(node.module);
        this.setState({ nodeData: resp, selectedNode: node });
    }

    search = (targetModule: string): TreeNode | null => {
        if (this.state.trees === undefined) { return null; }
        for (let treeRoot of this.state.trees) {
            const match = this.searchTreeNode(treeRoot, targetModule);
            if (match !== null) {
                return match;
            }
        }
        return null;
    }

    searchTreeNode = (curNode: TreeNode, targetModule: string): (TreeNode | null) => {
        if (curNode.module === targetModule) { return curNode; }

        if (curNode.children === undefined) { return null; }
        for (let child of curNode.children) {
            const found = this.searchTreeNode(child, targetModule);
            if (found !== null) {
                return found;
            }
        }

        return null;
    }

    onSelectEntity = (entityName: string) => {
        let matchNode = this.search(entityName);

        if (matchNode !== null) {
            this.onSelectLabel(matchNode);

            // Collapse everything below this node
            matchNode.collapsed = true;
            // Uncollapse everything above it
            let cur : TreeNode | undefined = matchNode.parent;
            while(cur) {
                cur.collapsed = false;
                cur = cur.parent;
            }
        }
    }

    loadTrees = async () => {
        const trees = await buildTrees();
        this.setState({ trees: trees });
    }

    async componentDidMount() {
        await this.loadTrees();
    }

    render() {
        return (
            <div className="metadata-view">
                <div className="metadata-view__left-pane">
                    {this.state.trees && this.state.trees!.map((treeRoot: TreeNode) => {
                        return (
                            <div>
                                <TreeView
                                    root={treeRoot}
                                    onClickNodeIcon={this.onSelectIcon}
                                    onClickNode={this.onSelectLabel}
                                    update={(root) => this.setState({ root: root })}
                                    selectedNode={this.state.selectedNode}
                                />
                            </div>
                        );
                    })}
                </div>
                <div className="metadata-view__right-pane">
                    <EntityViewer
                        moduleName={this.state.selectedNode ? this.state.selectedNode.module : "No Data"}
                        data={this.state.nodeData!}
                        selectEntity={this.onSelectEntity} />
                </div>
            </div>
        );
    }
}

export default MetadataView;