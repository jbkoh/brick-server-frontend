import React from "react";
import NotificationAlert from "react-notification-alert";
import {Button} from "reactstrap";
import "react-notification-alert/dist/animate.css";

export const Notification = () => {
    const notificationAlert: React.RefObject<any> = React.createRef();

    const notify = (place: string) => {
        const type = "primary";
        const options = {
            place: place,
            message: (
                <div>
                    <div>
                        Hello world
                    </div>
                </div>
            ),
            type: type,
            icon: "nc-icon nc-check-2",
            autoDismiss: 1
        };

        if (notificationAlert.current) {
            notificationAlert.current.notificationAlert(options);
        }
    };

    return (
        <>
            <div className="content superbad">
                <NotificationAlert ref={notificationAlert}/>
                <Button
                    block
                    color="primary"
                    onClick={() => notify("tr")}>
                    Top Left
                </Button>
            </div>
        </>
    );
};

export default Notification;
