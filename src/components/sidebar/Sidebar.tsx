import * as React from "react";
import {Nav} from "reactstrap";
import PerfectScrollbar from "perfect-scrollbar";
import * as logo from "assets/img/logo.png";
import SidebarLink from "./SidebarLink";
import {Route, routes, setActivatedAppsOnSidebar, setAppAdminOnSidebar} from "../../common/routes";
import {frontendURL, redirectOnError} from "../../common/api/api";
import {connect} from "react-redux";
import {MyReducerState} from "../../common/reducers";
import {Dispatch} from "redux";
import classNames from "classnames";
import * as api from "common/api/api";
import * as appApi from "common/api/app-api";
import * as actions from "common/actions";

type PropsFromRedux = {
    reloadTokens: () => void
    reloadActivatedApps: () => void
    reloadPendingApps: () => void
    reloadUserInfo: () => void
    setRoutes: (routes: Route[]) => void

    routes: Route[]
    activatedApps: string[]
    pendingApps: string[]
    isLoggedIn: boolean
}

type SidebarState = {
    expanded: boolean
    hoverMode: boolean
    // Whether expand or collapse was set by media query or user
    autoSet: boolean
}

type SidebarViewProps = PropsFromRedux & {
    activeColor: string
    bgColor: string
}

class SidebarView extends React.Component<SidebarViewProps, SidebarState> {
    private readonly sidebar: React.RefObject<HTMLDivElement>;
    private ps: PerfectScrollbar | null;

    constructor(props: SidebarViewProps) {
        super(props);
        this.sidebar = React.createRef();
        this.ps = null;

        this.state = {
            expanded: true,
            hoverMode: false,
            autoSet: true,
        }
    }

    async componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            if (this.sidebar.current) {
                this.ps = new PerfectScrollbar(this.sidebar.current, {
                    suppressScrollX: true,
                    suppressScrollY: false
                });
            }
        }

        try {
            await this.props.reloadActivatedApps();
            await this.props.reloadPendingApps();
            await this.props.reloadTokens();
            await this.props.reloadUserInfo();
        } catch (err) {
            redirectOnError(err);
        }

        // Listen for changes to auto open or close sidebar
        window.addEventListener("resize", this.handleResize.bind(this));
    }

    componentWillUnmount() {
        if (navigator.platform.indexOf("Win") > -1) {
            if (this.ps) {
                this.ps.destroy();
            }
        }
    }

    handleResize() {
        if (window.innerWidth < 993 && this.state.expanded) {
            this.collapseButtonClick(true);
        } else if(window.innerWidth > 993) {
            if (this.state.autoSet) {
                this.expandButtonClick(true);
            }
        }
    }

    onMouseEnter() {
        this.setExpand(true);
    }

    onMouseLeave() {
        this.setExpand(false);
    }

    setExpand(expand: boolean) {
        this.setState({expanded: expand});
    }

    collapseButtonClick(autoSet: boolean) {
        this.setState({hoverMode: true, expanded: false, autoSet: autoSet});
    }

    expandButtonClick(autoSet: boolean) {
        this.setState({hoverMode: false, expanded: true, autoSet: autoSet});
    }

    enterSidebar() {
        if (this.state.hoverMode) {
            this.setState({expanded: true});
        }
    }

    exitSidebar() {
        if (this.state.hoverMode) {
            this.setState({expanded: false});
        }
    }

    render() {
        setActivatedAppsOnSidebar(this.props.activatedApps);
        setAppAdminOnSidebar({pendingApps: this.props.pendingApps});

        this.props.setRoutes(routes);

        return (
            <div className="sidebar-wrapper"
                 onMouseLeave={this.exitSidebar.bind(this)}>
                <div className={classNames("sidebar",
                    {
                        "expanded": this.state.expanded,
                        "collapsed": !this.state.expanded
                    })}
                     data-color={this.props.bgColor}
                     data-active-color={this.props.activeColor}>

                    <div role="button"
                         className={classNames("collapse-button", "nc-icon",
                             {
                                 "nc-minimal-right": !this.state.expanded,
                                 "nc-minimal-left": this.state.expanded
                             })}
                         onClick={() => {
                             this.state.expanded ? this.collapseButtonClick(false) : this.expandButtonClick(false)
                         }}/>

                    <div className="logo">
                        <a href={frontendURL} className="simple-text logo-mini">
                            <div className="logo-img">
                                <img src={logo} alt="react-logo"/>
                            </div>
                        </a>

                        <a href={frontendURL} className="simple-text logo-normal">
                            {
                                this.state.expanded ? <span>Brick Dashboard</span> :
                                    <span>&nbsp;&nbsp;</span>
                            }
                        </a>
                    </div>
                    <div className="nav-wrapper"
                         onMouseEnter={this.enterSidebar.bind(this)}
                         ref={this.sidebar}>
                        <Nav>
                            {
                                this.props.routes.map((prop, key) => {
                                    return (
                                        <SidebarLink {...prop} key={key.toString()}
                                                     setExpanded={this.setExpand}
                                                     expanded={this.state.expanded}/>
                                    );
                                })
                            }
                          </Nav>
                    </div>
                </div>
            </div>
        );
    }
}

const mapState = (state: MyReducerState) => ({
    isLoggedIn: state.auth.isLoggedIn,
    activatedApps: state.activatedApps,
    pendingApps: state.pendingApps,
    routes: state.routes
});

const mapDispatch = (dispatch: Dispatch) => ({
    reloadTokens: api.reloadTokens(dispatch),
    reloadActivatedApps: appApi.reloadActivatedApps(dispatch),
    reloadPendingApps: appApi.reloadPendingApps(dispatch),
    reloadUserInfo: dispatch<any>(actions.reloadUserInfo),
    setRoutes: dispatch<any>(actions.setRoutes)
});

export const Sidebar = connect(mapState, mapDispatch)(SidebarView);

