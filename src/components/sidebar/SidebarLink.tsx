import React, {useState} from "react";
import {Collapse, NavLink} from "reactstrap";
import {Route} from "common/routes";
import {Link} from 'react-router-dom';

type SidebarLinkProps = Route & {
    expanded: boolean
    setExpanded: (expanded: boolean) => void
}

export default function SidebarLink(props: SidebarLinkProps) {
    // verifies if routeName is the one active (in browser input)
    function activeRoute(routeName: string) {
        return window.location.pathname.endsWith(routeName) ? "active" : "";
    }

    let [isOpen, setIsOpen] = useState(false);
    const text = props.expanded ? props.name : <span>&nbsp;&nbsp;</span>;

    if (!props.children) {
        return (
            <li
                className={activeRoute(props.path)}
                key={props.name}>
                <NavLink
                    tag={Link}
                    to={props.path}
                    className="nav-link">

                    {props.icon ?
                        <>
                            <i className={props.icon}/>
                            <p>{text}</p>
                        </> :
                        <p className="child">{text}</p>
                    }
                </NavLink>
            </li>
        );
    }

    return (
        <>
            <li
                onClick={() => setIsOpen(!isOpen)}
                className={activeRoute(props.path)}
                key={props.name}
            >
                <NavLink to={'#'}
                         tag={Link}
                         className="nav-link">

                    {props.icon && <i className={props.icon}/>}
                    <p>{text}</p>
                </NavLink>
            </li>

            {props.children && (
                <Collapse
                    isOpen={isOpen}
                    unmountOnExit
                >
                    {props.children.map(children => (
                        <ul key={children.name}>
                            <SidebarLink {...children} expanded={props.expanded}
                                         setExpanded={props.setExpanded}/>
                        </ul>
                    ))}
                </Collapse>
            )}
        </>
    );
}
