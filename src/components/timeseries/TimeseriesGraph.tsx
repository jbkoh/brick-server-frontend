import * as React from "react";
import {useState} from "react";
import {grafanaServerURL} from "common/api/grafana-api";
import {DEBUG, debug} from "common/utils";
import * as Types from "./TimeseriesView.d";

const urlForEmbed = (url: string) => {
    return url.replace("/d/", "/d-solo/");
}

export const TimeseriesGraph = (props: Types.TimeseriesChartProps) => {
    let defaultEndTime = new Date();
    let defaultStartTime = new Date(defaultEndTime.getMilliseconds() - 3600000);

    if (DEBUG) {
        defaultStartTime = new Date(1581161290.51 * 1000);
        defaultEndTime = new Date(1581161320.3 * 1000);
        debug("grafanaServerURL: ", grafanaServerURL);
    }

    const [timeEnd, setTimeEnd] = React.useState<Date>(props.timeEnd ? props.timeEnd : defaultEndTime);
    const [timeStart, setTimeStart] = React.useState<Date>(props.timeStart ? props.timeStart : defaultStartTime);

    const [embedSrc, setEmbedSrc] = React.useState<string>(`${grafanaServerURL}${urlForEmbed(props.url)}?orgId=1&from=${timeStart.getTime()}&to=${timeEnd.getTime()}&panelId=${props.panelId}`);

    const [embedWidth, setEmbedWidth] = useState(window.innerWidth / 2);
    const [embedHeight, setEmbedHeight] = useState(window.innerWidth * 2 / 9)

    // If Time is set, change url
    React.useEffect(() => {
        setEmbedSrc(`${grafanaServerURL}${urlForEmbed(props.url)}?orgId=1&from=${timeStart.getTime()}&to=${timeEnd.getTime()}&panelId=${props.panelId}`);
    }, [timeEnd, timeStart]);

    // Adjust panel size according to window width
    React.useEffect(() => {
        function handleResize() {
            setEmbedWidth(window.innerWidth / 2);
            setEmbedHeight(window.innerWidth * 2 / 9);
        }

        window.addEventListener('resize', handleResize);
        return () => {window.removeEventListener('resize', handleResize)};
    }, []);

    return (
        <div id="graph-wrapper">
            <iframe src={embedSrc} height={embedHeight.toString()} width={embedWidth.toString()}/>
        </div>
    )

}