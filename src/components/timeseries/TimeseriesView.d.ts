export type EntityListProps = {
    entities: string[]
    deleteEntity: (entityId: string) => void
}

export type TimeseriesChartProps = {
    entityList: string[],
    url: string,
    panelId: number,
    timeStart?: Date,
    timeEnd?: Date,
}