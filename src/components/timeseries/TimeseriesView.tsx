import * as React from "react";
import {Button, ListGroup, ListGroupItem, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {TimeseriesGraph} from "./TimeseriesGraph";
import {debug} from "common/utils";
import {SparqlPlayground} from '../sparql/SparqlPlayground';
import {GraphContainer, Dashboard, getDashboard, editDashboard, getGraphs, getDashboardJSON, getLatestPanelId} from "common/api/grafana-api";
import * as Types from "./TimeseriesView.d";

export const EntityList = (props: Types.EntityListProps) => {
    const entities = props.entities.map((e, idx) =>
        <ListGroupItem key={idx} className="item">
            <div>{e}</div>
            <i role="button" className="delete-button nc-icon nc-simple-remove"
               onClick={(evt) => props.deleteEntity(e)}/>
        </ListGroupItem>
    );

    return (
        <ListGroup className="entity-list">
            {entities}
        </ListGroup>
    );
};

export const TimeseriesView = () => {
    const [graphList, setGraphList] = React.useState<Array<GraphContainer>>([]);
    const [currentGraphIdx, setCurrentGraphIdx] = React.useState<number>(0);
    const [newEntityList, setNewEntityList] = React.useState<Array<string>>([]);
    const [newPanelTitle, setNewPanelTitle] = React.useState<string>("");
    const [modal, setModal] = React.useState(false);
    const [userDashboard, setUserDashboard] = React.useState<Dashboard | undefined>(undefined);
    const [dashboardLoaded, setDashboardLoaded] = React.useState(false);
    const [panelIdGen, setPanelIdGen] = React.useState(1);

    const removeGraph = (graphIdx: number) => {
        debug("removed graphIdx: " + graphIdx);
        // splice erases every element beyond graphIdx, so this is a manual elimination of graphIdx
        const newList = []
        for (let i = 0; i < graphList.length; i++) {
            if (i !== graphIdx) {
                newList.push(graphList[i]);
            }
        }
        setGraphList(newList);
    }

    const toggle = () => setModal(!modal);

    // resets newEntityList and currentGraphIdx which are used to update the appropriate graph, in this case the last graph (new graph)
    const addGraph = () => {
        setNewEntityList([]);
        setCurrentGraphIdx(graphList.length);
        toggle();
    }

    // actually executes the changes stored in newEntityList and currentGraphIdx, whether its by adding a new one or editing an existing one
    const editGraph = () => {
        console.log("Current graph index: " + currentGraphIdx);
        if (currentGraphIdx === graphList.length) {
            setGraphList([...graphList,
                {
                    panelId: panelIdGen,
                    entityList: newEntityList,
                    panelTitle: newPanelTitle,
                }
            ]);
        } else {
            debug("editting graph", graphList);
            const newGraphList = graphList;
            newGraphList[currentGraphIdx].panelId = panelIdGen;
            newGraphList[currentGraphIdx].entityList = newEntityList;
            newGraphList[currentGraphIdx].panelTitle = newPanelTitle;
            setGraphList([...newGraphList]);
        }

        // generate new id after each edit so the key of each panel component changes and reloads the embed real-time
        setPanelIdGen(panelIdGen + 1);
        toggle();
    }

    // Adds entityId if clicked entity does not exist in list
    const handleClickCell = (entityId: string) => {
        setNewEntityList(entityList => {
            if (entityList.includes(entityId)) {
                return entityList;
            }
            return [...entityList, entityId]
        });
    };

    // Removes clicked entity from list
    const deleteEntity = (e: string) => {
        setNewEntityList(entityList => entityList.filter(entity => entity !== e));
    };

    // When graphList is updated, update grafana panels
    React.useEffect(() => {
        console.log("Requesting /grafana");
        const postData = async () => {
            const res = await editDashboard(userDashboard!, graphList);
        }
        if (dashboardLoaded) {
            postData();
        }
        debug("Graph List: ", graphList);
    }, [graphList]);

    // When page first loads, load dashboard data
    React.useEffect(() => {
        const fetchInitialData = async () => {
            const newDashboard = await getDashboard();
            console.log("getDashboard response: ", newDashboard);
            setUserDashboard(newDashboard);
        }
        fetchInitialData();

    }, [])

    // When dashboard data is loaded, get dashboard json to load panels and get the latest panel id from previous session
    React.useEffect(() => {
        const fetchGraphs = async () => {
            const res = await getDashboardJSON();
            const previousGraphList = getGraphs(res);
            const latestPanelId = getLatestPanelId(res);
            debug("Graphs from previous session", previousGraphList);
            setGraphList(previousGraphList);
            setPanelIdGen(latestPanelId + 1);
        }

        debug("", userDashboard);
        if (userDashboard !== undefined) {
            setDashboardLoaded(true);
            fetchGraphs();
        } else {
            setDashboardLoaded(false);
        }
    }, [userDashboard])

    return (
        <div className="timeseries">
            <Button
                className="toggle-button btn-primary"
                onClick={addGraph}
                disabled={!dashboardLoaded}>
                Add
            </Button>
            <Modal isOpen={modal} toggle={toggle} className="edit-modal">
                <ModalHeader toggle={toggle}></ModalHeader>
                <ModalBody>
                    <div>
                        <input type="text"
                               className="title-input"
                               value={newPanelTitle}
                               placeholder="Panel Title"
                               onChange={e => setNewPanelTitle(e.target.value)}/>
                    </div>
                    <div className="playground">
                        <SparqlPlayground selectable={true} onClickCell={handleClickCell}/>
                    </div>
                    {newEntityList.length > 0 &&
                    <div className="output">
                        <div className="output-label">
                            Selected Entities
                        </div>
                        <EntityList deleteEntity={deleteEntity} entities={newEntityList}/>
                    </div>
                    }
                </ModalBody>
                <ModalFooter>
                    <Button className="finish-edit-button btn-primary" onClick={editGraph}>Done</Button>
                    <Button className="cancel-edit-button btn-secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
            <div className="graph-list">
                {dashboardLoaded && graphList.map((container: GraphContainer, idx: number) => {
                    return (
                        <div className="graph-wrapper" key={container.panelId}>
                            <div className="graph-header">
                                <i role="button" className="graph-button nc-icon nc-ruler-pencil"
                                    onClick={() => {setCurrentGraphIdx(idx); toggle();}} />
                                <i role="button" className="graph-button nc-icon nc-simple-remove"
                                    onClick={() => removeGraph(idx)}/>
                            </div>
                            <TimeseriesGraph
                                entityList={container.entityList}
                                url={userDashboard!.url}
                                panelId={container.panelId}
                            />
                        </div>
                    )
                })}
            </div>
        </div>
    );
};

export default TimeseriesView;
