import * as React from "react";
import {Container, Row} from "reactstrap";
import {frontendURL} from "common/api/api";

type FooterProps = {
    default: boolean,
    fluid: boolean
}

class Footer extends React.Component<FooterProps> {
    render() {
        return (
            <footer className={"footer" + (this.props.default ? " footer-default" : "")}>
                <Container fluid={this.props.fluid}>
                    <Row>
                        <nav className="footer-nav">
                            <ul>
                                <li>
                                    <a href={frontendURL}>
                                        Brick Server
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Blog
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Licenses
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div className="credits ml-auto">
                            <div className="copyright">
                                &copy; {new Date().getFullYear()} made with{" "}
                                <i className="fa fa-heart heart"/> by Brick Server
                            </div>
                        </div>
                    </Row>
                </Container>
            </footer>
        );
    }
}

export default Footer;
