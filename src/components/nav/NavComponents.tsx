import {
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from "reactstrap";
import * as React from 'react';
import * as api from "common/api/api";
import {useSelector} from "react-redux";
import {MyReducerState} from "../../common/reducers";

type ActionOption = {
    dropdownToggle: () => void
    dropdownOpen: boolean
}

export const ActionOption = ({dropdownOpen, dropdownToggle}: ActionOption) => (
    <Dropdown
        nav
        isOpen={dropdownOpen}
        toggle={dropdownToggle}>
        <DropdownToggle caret nav>
            <i className="nc-icon nc-bell-55"/>
            <p>
                <span className="d-lg-none d-md-block">Some Actions</span>
            </p>
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem tag="a">Action</DropdownItem>
            <DropdownItem tag="a">Another Action</DropdownItem>
            <DropdownItem tag="a">Something else here</DropdownItem>
        </DropdownMenu>
    </Dropdown>
);

export const SearchBar = () => (
    <form>
        <InputGroup className="no-border">
            <Input placeholder="Search..."/>
            <InputGroupAddon addonType="append">
                <InputGroupText>
                    <i className="nc-icon nc-zoom-split"/>
                </InputGroupText>
            </InputGroupAddon>
        </InputGroup>
    </form>
);


export const LoginButton = () => {
    let {isLoggedIn} = useSelector((state: MyReducerState) => state.auth);
    return (isLoggedIn ?
            <a href={api.logoutURL} className="nav-link btn btn-sm btn-primary">
                Log out
            </a>
            :
            <a href={api.loginURL} className="nav-link btn btn-sm btn-primary">
                Log in
            </a>
    );
};
