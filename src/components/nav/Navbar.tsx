import * as React from "react";
import {RouteComponentProps} from "react-router-dom";
import {Nav, Navbar, NavbarBrand, NavItem} from "reactstrap";

import {Route} from "common/routes";
import {ActionOption, LoginButton, SearchBar} from "./NavComponents";
import {MyReducerState} from "common/reducers";
import {connect} from "react-redux";


type HeaderState = {
    dropdownOpen: boolean
    color: string
}

type HeaderPropsFromRedux = {
    routes: Route[]
}

class HeaderView extends React.Component<RouteComponentProps & HeaderPropsFromRedux, HeaderState> {
    private readonly sidebarToggle: React.RefObject<HTMLButtonElement>;

    constructor(props: RouteComponentProps & HeaderPropsFromRedux) {
        super(props);
        this.state = {
            dropdownOpen: false,
            color: "transparent"
        };
        this.sidebarToggle = React.createRef();
    }

    dropdownToggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    };

    render() {
        return (
            <Navbar expand="lg">
                <div className="nav-wrapper">
                    <NavbarBrand href="/">Brick Server</NavbarBrand>
                    <Nav navbar className="action-buttons">
                        <SearchBar/>
                        <ActionOption dropdownOpen={this.state.dropdownOpen}
                                      dropdownToggle={this.dropdownToggle}/>
                        <NavItem>
                            <LoginButton/>
                        </NavItem>
                    </Nav>
                </div>
            </Navbar>
        );
    }
}

const mapState = (state: MyReducerState) => ({
    routes: state.routes
});

export const MyNavbar = connect(mapState)(HeaderView);

