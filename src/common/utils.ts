export const DEBUG = process.env.REACT_APP_DEBUG === "true";

export const debug = (msg: string, ...rest: any) => {
    if(DEBUG) {
        console.log(msg, rest);
    }
};
