import {
    LOGIN,
    LOGOUT,
    SET_ACTIVATED_APPS,
    SET_PENDING_APPS,
    SET_ROUTES,
    SET_TOKENS,
    SET_USER_INFO
} from "./actionTypes";
import {Dispatch} from "redux";
import * as api from "common/api/api";
import * as appApi from "common/api/app-api";
import {redirectOnError} from "common/api/api";
import {LoginToken} from "./LoginToken";
import {debug} from "./utils";
import {Route} from "./routes";

export const fetchTokens = () => (dispatch: Dispatch) => {
    return api.getTokens().then(tokens => {
        dispatch({
            type: SET_TOKENS,
            tokens: tokens
        });
    }).catch(err => {
        redirectOnError(err);
    });
};

export const fetchActivatedApps = () => (dispatch: Dispatch) => {
    return appApi.getActivatedApps().then(apps => {
        dispatch({
            type: SET_ACTIVATED_APPS,
            activatedApps: apps
        });
    }).catch(err => {
        redirectOnError(err);
    });
};

export const fetchPendingApps = () => (dispatch: Dispatch) => {
    return appApi.getPendingApps().then(apps => {
        dispatch({
            type: SET_PENDING_APPS,
            pendingApps: apps
        });
    }).catch(err => {
        redirectOnError(err);
    });
};

export const setRoutes = (dispatch: Dispatch) => (routes: Route[]) => {
    dispatch({
        type: SET_ROUTES,
        routes: routes
    });
};

export const login = (dispatch: Dispatch) => (token: LoginToken) => {
    dispatch({
        type: LOGIN,
        userId: token.user_id
    });
};

export const logout = (dispatch: Dispatch) => () => {
    dispatch({
        type: LOGOUT,
    });
};

export const reloadUserInfo = (dispatch: Dispatch) => () => {
    return appApi.getUserInfo().then(info => {
        debug(info);
        dispatch({
            type: SET_USER_INFO,
            isApproved: info["is_approved"],
            isAdmin: info["is_admin"],
        });
    }).catch(err => {
        console.log(err);
        console.warn("User is not logged in. No user info to load.");
        console.warn(`It's possible that the current user is only a brick user, 
        not a playground user. We can only get data for playground users.`);
    });
};
