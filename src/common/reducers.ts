import {
    AppActions,
    AuthActions,
    LOGIN,
    LOGOUT, RouteActions,
    SET_ACTIVATED_APPS,
    SET_PENDING_APPS, SET_ROUTES,
    SET_TOKENS,
    TokenActions
} from "./actionTypes";
import {RawAppToken} from "./AppToken";
import {AuthInitialState} from "../index";
import {Route} from "./routes";

export type AppToken = {
    token: string
    appName: string
    expiration: number
}

export type MyReducerState = {
    tokens: RawAppToken[]
    auth: AuthFields
    activatedApps: string[]
    pendingApps: string[]
    routes: Route[]
}

export const tokens = (state: RawAppToken[] = [], action: TokenActions) => {
    switch (action.type) {
        case SET_TOKENS:
            return action.tokens;
        default:
            return state;
    }
};

export const activatedApps = (state: string[] = [], action: AppActions) => {
    switch (action.type) {
        case SET_ACTIVATED_APPS:
            return action.activatedApps;
        default:
            return state;
    }
};

export const pendingApps = (state: string[] = [], action: AppActions) => {
    switch (action.type) {
        case SET_PENDING_APPS:
            return action.pendingApps;
        default:
            return state;
    }
};

export const routes = (state: Route[] = [], action: RouteActions) => {
    switch (action.type) {
        case SET_ROUTES:
            return action.routes;
        default:
            return state;
    }
};

export type AuthFields = {
    userId: string
    isLoggedIn: boolean
    isApproved: boolean
    isAdmin: boolean
}

export const auth = (state = AuthInitialState, action: AuthActions) => {
    switch (action.type) {
        case LOGIN:
            return Object.assign({}, state, {userId: action.userId, isLoggedIn: true});
        case LOGOUT:
            return Object.assign({}, state, {userId: "", isLoggedIn: false});
        default:
            return state;
    }
};
