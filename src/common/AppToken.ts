export type RawAppToken = {
    token: string
    name: string
    exp: number
}

export const convertToAppToken = (data: RawAppToken[]) => {
    return data.map((r: RawAppToken, key: number) =>
        ({id: key, token: r.token, appName: r.name, expiration: r.exp}));
};
