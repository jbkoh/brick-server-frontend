import {UserProfile} from "pages/UserProfile";
import {ComponentClass, FunctionComponent} from "react";
import {MetadataExplorer} from "../pages/data_exploration/MetadataExplorer";
import {TimeseriesExploration} from "../pages/data_exploration/TimeseriesExploration";
import {TokenManagement} from "../pages/token/TokenManagement";
import {SparqlExploration} from "pages/data_exploration/SparqlExploration";
import {AvailableApps} from "../pages/app_admin/AvailableApps";
import {PendingApps} from "../pages/app_admin/PendingApps";
import {ActivateApps} from "../pages/app_admin/ActivateApps";

const APP_ADMIN_SLOT = 4;
const ACTIVATED_APPS_SLOT = 5;

export type Route = {
    name: string,
    path: string,
    icon?: string,
    component?: ComponentClass | FunctionComponent,
    children?: Route[]
}

export const AppParent = {
    path: "/apps",
    name: "Apps",
    icon: "nc-icon nc-app"
};

export const setActivatedAppsOnSidebar = (activatedApps: string[]) => {
    const ret = Object.assign(AppParent);
    ret.children = activatedApps.map(app => ({
        name: app,
        path: `/apps/${app}`
    }));
    routes[ACTIVATED_APPS_SLOT] = ret;
};

interface AppAdminProps {
    pendingApps: string[]
}

export const setAppAdminOnSidebar = (props: AppAdminProps) => {
    const children = [];
    children.push({
        path: "/available",
        name: "Available Apps",
        component: AvailableApps,
    });
    // Only include pending apps component if we have pending apps
    if (props.pendingApps.length > 0) {
        children.push({
            path: "/pending",
            name: "Pending Apps",
            component: PendingApps,
        })
    }
    children.push({
        path: "/activate",
        name: "Activate Apps",
        component: ActivateApps,
    });

    const ret = routes.slice();
    routes[APP_ADMIN_SLOT] =
        {
            path: "/app-admin",
            name: "App Admin",
            icon: "nc-icon nc-tile-56",
            children: children,
        };

    return ret;
};

export const routes: Route[] = new Array(6);
routes[0] =
    {
        path: "/user-profile",
        name: "User Profile",
        icon: "nc-icon nc-circle-10",
        component: UserProfile
    };
routes[1] =
    {
        path: "/tokens",
        name: "Tokens",
        icon: "nc-icon nc-tablet-2",
        component: TokenManagement,
    };
routes[2] =
    {
        path: "/metadata",
        name: "Metadata",
        icon: "nc-icon nc-zoom-split",
        children: [
            {
                path: "/brick-studio",
                name: "Brick Studio",
                component: MetadataExplorer,
            },
            {
                path: "/sparql",
                name: "SPARQL",
                component: SparqlExploration,
            },
        ],
    };
routes[3] =
    {
        path: "/timeseries",
        name: "Time Series",
        icon: "nc-icon nc-chart-bar-32",
        children: [
            {
                path: "/data-plot",
                name: "Data Plot",
                component: TimeseriesExploration,
            }
        ],
    };

export const flatRoutes = (routes: Route[]) =>
    routes.flatMap(r => {
        if (r.children) {
            return [r, ...r.children]
        }
        return [r]
    });
