import {RawAppToken} from "./AppToken";
import {Route} from "./routes";

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export const SET_USER_INFO = "SET_USER_INFO";

export const SET_TOKENS = "SET_TOKENS";
export const SET_ACTIVATED_APPS = "SET_ACTIVATED_APPS";

export const SET_PENDING_APPS = "SET_PENDING_APPS";

export const SET_ROUTES = "SET_ROUTES";

interface SetActivatedAppsAction {
    type: typeof SET_ACTIVATED_APPS
    activatedApps: string[]
}

interface SetPendingAppsAction {
    type: typeof SET_PENDING_APPS
    pendingApps: string[]
}

interface SetTokenAction {
    type: typeof SET_TOKENS
    tokens: RawAppToken[]
}

interface LoginAction {
    type: typeof LOGIN
    userId: string
}

interface SetUserInfoAction {
    type: typeof SET_USER_INFO
    isApproved: boolean
    isAdmin: boolean
}

interface LogoutAction {
    type: typeof LOGOUT
}

interface SetRouteAction {
    type: typeof SET_ROUTES
    routes: Route[]
}

export type RouteActions = SetRouteAction;
export type AppActions = SetActivatedAppsAction | SetPendingAppsAction
export type TokenActions = SetTokenAction
export type AuthActions = LoginAction | LogoutAction | SetUserInfoAction

