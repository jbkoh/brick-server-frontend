import * as React from 'react';
import {Redirect, Route as ReactRoute, RouteProps} from "react-router";
import {useSelector} from "react-redux";
import {MyReducerState} from "./reducers";

export const PrivateRoute = (props: RouteProps) => {
    const {isLoggedIn} = useSelector((state: MyReducerState) => state.auth);
    return isLoggedIn ?
        <ReactRoute {...props} />
        :
        // Redirect every path to go to NotLoggedIn page
        <Redirect to="/needs-login"/>
};
