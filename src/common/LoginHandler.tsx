import * as React from 'react';
import {useEffect, useState} from 'react';
import {Redirect} from "react-router";
import {useLocation} from 'react-router-dom';
import {useDispatch} from "react-redux";
import * as actions from "./actions";
import {clearToken, decodeToken, storeToken} from "./LoginToken";
import * as api from "common/api/api";
import {Card, CardBody, CardFooter, CardHeader, CardTitle, Col, Container, Row, Spinner} from "reactstrap";


export const LoginHandle = () => {
    const dispatch = useDispatch();
    const query = new URLSearchParams(useLocation().search);
    const token = query.get("app_token");
    const login = actions.login(dispatch);

    if (token) {
        storeToken(token);
        const loginToken = decodeToken(token);
        login(loginToken);
    }

    return (<Redirect to={"/"}/>)
};

export const LogoutHandle = () => {
    const dispatch = useDispatch();
    const logout = actions.logout(dispatch);
    logout();
    clearToken();

    return (<Redirect to={"/"}/>)
};

type AppHandleProps = {
    appName: string
}

export const AppHandle = (props: AppHandleProps) => {
    const [url, setURL] = useState("");

    useEffect(() => {
        api.getAppHTML(props.appName).then(url => setURL(url))
            .catch(() => console.error(`Failed to get app html url for app ${props.appName}`));

        return () => {
            setURL("");
        }
    }, [props.appName]);


    return (
        <div className="app-handle content">
            <Container>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h5">{props.appName}</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <div className="app">
                                    <Spinner className={url === "" ? "spinner enabled" : "spinner disabled"}
                                             color="dark" size="lg"/>
                                    <iframe className={url !== "" ? "frame enabled" : "frame disabled"}
                                            title="app"
                                            src={url}/>
                                </div>
                            </CardBody>
                            <CardFooter>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>);
};
