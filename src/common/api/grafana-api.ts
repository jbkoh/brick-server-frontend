import {authFetch, appBaseURL, HTTPError} from "./api";
import {debug} from "common/utils";

export const grafanaURL = `${appBaseURL}/grafana`;
export const grafanaServerURL = process.env.REACT_APP_GRAFANA_URL;
const grafanaKey = process.env.REACT_APP_GRAFANA_API_KEY;
const dashboardURL = `${grafanaServerURL}/api/dashboards/uid`;

export type GraphContainer = {
    panelId: number,
    entityList: string[]
    panelTitle: string,
}
export type Dashboard = {
    uid: string
    grafana_id: number
    url: string
}

const buildPanelJson = (container: GraphContainer) => {
    let entityJson: Array<string> = [];
    for (let i = 0; i < container.entityList.length; i++) {
        let newJson = `{
      "refId": "${container.entityList[i]}",
      "format": "time_series",
      "timeColumn": "\\"time\\"",
      "metricColumn": "uuid",
      "group": [],
      "where": [
        {
          "type": "macro",
          "name": "$__timeFilter",
          "params": []
        },
        {
          "type": "expression",
          "datatype": "text",
          "name": "",
          "params": [
            "uuid",
            "=",
            "'${container.entityList[i]}'"
          ]
        }
      ],
      "select": [
        [
          {
            "type": "column",
            "params": [
              "number"
            ]
          }
        ]
      ],
      "rawQuery": false,
      "table": "brick_data",
      "timeColumnType": "timestamp"
    }`
        if (i < container.entityList.length - 1) {
          newJson.concat(",");
        }
        entityJson.push(newJson);
    }
    let panelJson: string = `{
  "type": "graph",
  "title": "${container.panelTitle}",
  "gridPos": {
    "x": 0,
    "y": 0,
    "w": 12,
    "h": 9
  },
  "id": ${container.panelId},
  "targets": [
      ${entityJson}
  ],
  "options": {
    "dataLinks": []
  },
  "datasource": "BrickData",
  "fieldConfig": {
    "defaults": {
      "custom": {}
    },
    "overrides": []
  },
  "renderer": "flot",
  "yaxes": [
    {
      "label": null,
      "show": true,
      "logBase": 1,
      "min": null,
      "max": null,
      "format": "short"
    },
    {
      "label": null,
      "show": true,
      "logBase": 1,
      "min": null,
      "max": null,
      "format": "short"
    }
  ],
  "xaxis": {
    "show": true,
    "mode": "time",
    "name": null,
    "values": [],
    "buckets": null
  },
  "yaxis": {
    "align": false,
    "alignLevel": null
  },
  "lines": true,
  "fill": 1,
  "linewidth": 1,
  "dashLength": 10,
  "spaceLength": 10,
  "pointradius": 2,
  "legend": {
    "show": true,
    "values": false,
    "min": false,
    "max": false,
    "current": false,
    "total": false,
    "avg": false
  },
  "nullPointMode": "null",
  "tooltip": {
    "value_type": "individual",
    "shared": true,
    "sort": 0
  },
  "aliasColors": {},
  "seriesOverrides": [],
  "thresholds": [],
  "timeRegions": [],
  "fillGradient": 0,
  "dashes": false,
  "hiddenSeries": false,
  "points": false,
  "bars": false,
  "stack": false,
  "percentage": false,
  "steppedLine": false,
  "timeFrom": null,
  "timeShift": null
}`;
    return panelJson;
}
export const createDashboard = async () => {

    const endpoint = grafanaURL;
    const grafanaHeader = new Headers();
    grafanaHeader.append("Content-Type", "application/json");

    const stream = await authFetch(endpoint, {
        method: "POST",
        headers: grafanaHeader,
    });
    return await stream.json();
}

export const getDashboard = async () => {
    const endpoint = grafanaURL;
    const grafanaHeader = new Headers();
    grafanaHeader.append("Content-Type", "application/json");

    let dashboardExists = true;

    let stream;

    stream = await authFetch(endpoint, {
        method: "GET",
        headers: grafanaHeader,
    }).then(res => {
        if (res.status == 404) {
            dashboardExists = false;
        }
        return res;
    }).catch(err => {
        console.log("Failed to get dashboard: " + err);
    });

    if (!dashboardExists) {
        stream = await createDashboard();
    }

    return stream.json();
}

export const editDashboard = async (dashboard: Dashboard, graphList: Array<GraphContainer>) => {
    let panelJson: Array<string> = [];
    let idList: string[] = [];
    for (let i = 0; i < graphList.length; i++) {
        let newJson = buildPanelJson(graphList[i]);
        idList.push(graphList[i].panelId.toString());
        debug("panel id: ", graphList[i].panelId);
        if (i < graphList.length - 1) {
          newJson.concat(",")
        }
        panelJson.push(newJson);
    }

    let dashboardJson = `{
 "dashboard":  {
        "id": ${dashboard.grafana_id},
        "panels": [
            ${panelJson}
        ],
        "title": "",
        "uid": "${dashboard.uid}",
        "version": 1
    }
}`;

    const endpoint = grafanaURL;
    const grafanaHeader = new Headers();
    grafanaHeader.append("Content-Type", "application/json");

    debug("", dashboardJson);

    const stream = await authFetch(endpoint, {
        method: "POST",
        headers: grafanaHeader,
        body: dashboardJson,
    });
    return await stream.json();
}

export const getDashboardJSON = async () => {
  const endpoint = `${grafanaURL}/details`;
  const grafanaHeader = new Headers();
  grafanaHeader.append("Content-Type", "application/json");
  grafanaHeader.append("Accept", "application/json");

  const stream = await authFetch(endpoint, {
    method: "GET",
    headers: grafanaHeader,
  });
  return await stream.json();
}

export const getLatestPanelId = (dashboardJSON: any) => {
  const panels = dashboardJSON["dashboard"]["panels"];
  if (panels.length == 0) {
    return 1;
  } else {
    return panels[panels.length - 1]["id"];
  }
}

export const getGraphs = (dashboardJSON: any) => {
  const graphList: Array<GraphContainer> = [];
  debug("dashboardJSON: ", dashboardJSON);
  for (let i = 0; i < dashboardJSON["dashboard"]["panels"].length; i++) {
    const entityList: Array<string> = [];
    for (let j = 0; j < dashboardJSON["dashboard"]["panels"][i]["targets"].length; j++) {
      entityList.push(dashboardJSON["dashboard"]["panels"][i]["targets"][j]["refId"]);
    }
    const newGraph = {
      panelId: dashboardJSON["dashboard"]["panels"][i]["id"],
      entityList: entityList,
      panelTitle: dashboardJSON["dashboard"]["panels"][i]["title"]
    }
    graphList.push(newGraph);
  }

  return graphList;
}