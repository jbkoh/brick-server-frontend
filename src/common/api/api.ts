import {Dispatch} from "redux";
import {fetchTokens} from "../actions";
import {clearToken, getRawStoredToken} from "../LoginToken";
import {RawAppToken} from "../AppToken";

export const backendURL = process.env.REACT_APP_BACKEND_URL;
export const frontendURL = process.env.REACT_APP_FRONTEND_URL;

export const loginURL = `${backendURL}/auth/login`;
export const logoutURL = `${frontendURL}/logout`;
export const registerURL = `${backendURL}/auth/register`;
export const appBaseURL = `${backendURL}/brickapi/v1`;
export const userURL = `${appBaseURL}/user`;
export const availableAppsURL = `${appBaseURL}/market_apps`;
export const approvedAppsURL = `${appBaseURL}/apps`;
export const activateAppsURL = `${appBaseURL}/user/apps`;
export const approveAppURL = `${appBaseURL}/admin/app_approval`;
export const pendingAppsURL = `${appBaseURL}/admin/app_approval`;
export const stageAppURL = `${appBaseURL}/apps`;

export const getAppHTML = async (appName: string) => {
    const endpoint = `${backendURL}/auth/app_login/${appName}`;
    const res = await authFetch(endpoint, {method: "GET"});
    return `${backendURL}${await res.text()}`;
};

const tokenURl = `${backendURL}/auth/app_tokens`;

export class HTTPError extends Error {
    statusCode?: number;

    constructor(message?: string, statusCode?: number) {
        super(message);
        this.statusCode = statusCode;
        this.name = "HTTPError";
    }

    toString() {
        return JSON.stringify(this);
    }
}


export const authFetch = async (input: RequestInfo, init?: RequestInit) => {
    const token = getRawStoredToken();
    // No token stored, throw 405 error to tell client not to redirect to login
    // Note, this is different than 401, which we should get from the server on not authenticated
    if (token == null) {
        throw new HTTPError("User is not authenticated.", 405);
    }

    const auth = {Authorization: `Bearer ${token}`};
    if (init && init.headers) {
        init.headers = {...init.headers, ...auth};
    } else if (init && !init.headers) {
        init.headers = auth;
    } else {
        init = {headers: auth};
    }

    const res = await fetch(input, init);
    if (!res.ok && init.redirect !== "manual" && res.status !== 409) {
        console.error("An error occurred while fetching");
        throw new HTTPError(`Encountered error: '${res.status} ${res.statusText}' when querying ${input}`, res.status);
    }
    return res;
};

/**
 * Generates a token given the app name and expiry time
 *
 * @param appName application name that wants the token
 * @param expiryTime when the token will expire
 * @return Returns a tuple of (success, errorMsg), where errorMsg is set if request failed
 */
export const genToken = async (appName: string, expiryTime: string) => {
    const endpoint = `${tokenURl}?app_name=${appName}&token_lifetime=${expiryTime}`;
    return await authFetch(endpoint, {method: "POST"});
};

export const deleteToken = async (token: string) => {
    const endpoint = `${tokenURl}/${token}`;
    return await authFetch(endpoint, {method: "DELETE"});
};

export const getTokens = async (): Promise<RawAppToken[]> => {
    const endpoint = tokenURl;

    const stream = await authFetch(endpoint, {method: "GET"});
    return await stream.json();
};

/**
 * Redirects the user to the login URL
 */
export const redirectOnError = (err: HTTPError) => {
    // TODO Switch on different errors more cases in the future, 500, e.g
    // TODO change 404 on user doesn't exist to 405
    switch (err.statusCode) {
        // Redirect to login on unauthorized
        case 401:
            clearToken();
            window.location.assign(loginURL);
            break;
        default:
            console.log("%c Failed for some reason", "color: red", err.message);
    }
};

export const reloadTokens = (dispatch: Dispatch) => async () => dispatch<any>(fetchTokens());
