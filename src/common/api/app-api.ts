import {
    activateAppsURL,
    approveAppURL,
    approvedAppsURL,
    authFetch,
    availableAppsURL,
    pendingAppsURL,
    stageAppURL,
    userURL
} from "./api";
import {AppData} from "pages/app_admin/App.d";
import {Dispatch} from "redux";
import {fetchActivatedApps, fetchPendingApps} from "../actions";

export const activateApp = async (name: string): Promise<void> => {
    const endpoint = activateAppsURL;
    const data = {app_name: name};
    const stream = await authFetch(endpoint, {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });
    return await stream.json();
};

export const approveApp = async (name: string): Promise<void> => {
    const endpoint = approveAppURL;
    const data = {app_name: name};
    const stream = await authFetch(endpoint, {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });
    return await stream.json();
};

export const stageApp = async (name: string): Promise<void> => {
    const endpoint = stageAppURL;
    const data = {app_name: name, app_lifetime: 30000};
    const stream = await authFetch(endpoint, {
        method: "POST",
        headers: {'Content-Type': 'application/json'}, body: JSON.stringify(data)
    });
    return await stream.json();
};

export const getUserInfo = async () => {
    const endpoint = userURL;
    const stream = await authFetch(endpoint, {
        method: "GET",
    });
    return await stream.json();
};

export const getAvailableApps = async (): Promise<string[]> => {
    const endpoint = availableAppsURL;
    try {
        const stream = await authFetch(endpoint, {
            method: "GET",
            redirect: "follow"
        });
        const json = await stream.json();
        return json["market_apps"];
    } catch (err) {
        console.warn("Could not get available apps");
        console.error(err);
        return [];
    }
};


export const getPendingApps = async (): Promise<string[]> => {
    const endpoint = pendingAppsURL;
    try {
        const stream = await authFetch(endpoint, {
            method: "GET",
        });
        const json = await stream.json();
        return json;
    } catch (err) {
        console.warn("Could not get pending apps");
        console.error(err);
        return [];
    }
};

export const getActivatedApps = async (): Promise<string[]> => {
    const endpoint = activateAppsURL;
    try {
        const stream = await authFetch(endpoint, {
            method: "GET",
        });
        const res = await stream.json();
        return res['activated_apps'];
    } catch (err) {
        console.warn("Could not get activated apps");
        console.error(err);
        return [];
    }
};

export const getApprovedApps = async (): Promise<string[]> => {
    const endpoint = approvedAppsURL;
    try {
        const stream = await authFetch(endpoint, {
            method: "GET",
        });
        const json: any[] = await stream.json();
        return json
            .filter(e => e['is_approved'])
            .map(e => e['name']);
    } catch (err) {
        console.warn("Could not get activated apps");
        console.error(err);
        return [];
    }
};

export const getAppData = async (appName: string): Promise<AppData> => {
    const endpoint = `${availableAppsURL}/${appName}`;
    try {
        const stream = await authFetch(endpoint, {
            method: "GET",
            redirect: "follow"
        });
        const json = await stream.json();
        console.log(json);
        return json;
    } catch (err) {
        console.warn(`No description found for app ${appName}`);
        return {
            description: "No description available.",
            name: appName,
        };
    }
};

export const reloadActivatedApps = (dispatch: Dispatch) => async () => dispatch<any>(fetchActivatedApps());
export const reloadPendingApps = (dispatch: Dispatch) => async () => dispatch<any>(fetchPendingApps());
