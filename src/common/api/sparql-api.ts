import {authFetch, appBaseURL} from "./api";

export const queryURL = `${appBaseURL}/rawqueries`;
export const sparqlURL = `${queryURL}/sparql`;
export const entityURL = `${appBaseURL}/entities`;
export const timeseriesURL = `${appBaseURL}/data/timeseries`

export const getSparqlQuery = async (queryString: string) => {
    const endpoint = sparqlURL;
    const sparqlHeader = new Headers();
    sparqlHeader.append("Content-Type", "application/sparql-query");

    const stream = await authFetch(endpoint, {
        method: "POST",
        headers: sparqlHeader,
        body: queryString
    });
    return await stream.json();
};

export const getEntity = async (entityId: string) => {
    const endpoint = `${entityURL}/${entityId}`;

    const entityHeader = new Headers();
    entityHeader.append("Content-Type", "application/json");

    const stream = await authFetch(endpoint, {
        method: "GET",
        headers: entityHeader,
    });
    return await stream.json();
};

export const getTimeseries = async (entityId: string, startTime: number, endTime: number) => {
    const endpoint = `${timeseriesURL}/${entityId}?start_time=${startTime}&end_time=${endTime}`;

    const entityHeader = new Headers();
    entityHeader.append("Content-Type", "application/json");

    const stream = await authFetch(endpoint, {method: "GET",
        headers: entityHeader,
    });
    return await stream.json();
}
