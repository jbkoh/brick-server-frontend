import {getRawStoredToken, getStoredToken} from "./LoginToken";
import { debug } from "console";

const backendURL = process.env.REACT_APP_BACKEND_URL;

export const frontendURL = process.env.REACT_APP_FRONTEND_URL;
export const loginURL = `${backendURL}/auth/login`;
export const logoutURL = `${frontendURL}/logout`;
export const brickURL = `${backendURL}/brickapi/v1`
export const registerURL = `${backendURL}/auth/register`;
export const queryURL = `${brickURL}/rawqueries`
export const sparqlURL = `${queryURL}/sparql`
export const entityURL = `${brickURL}/entities`
export const timeseriesURL = `${brickURL}/data/timeseries`

export const grafanaURL = process.env.GRAFANA_URL;

const tokenURL = `${backendURL}/auth/app_tokens`;

const authFetch = async (input: RequestInfo, init?: RequestInit) => {
    const token = getRawStoredToken();
    if (token == null) {
        throw new Error("User is not authenticated.");
    }

    const auth = {Authorization: `Bearer ${token}`};
    if (init && init.headers) {
        init.headers = {...init.headers, ...auth};
    } else if (init && !init.headers) {
        init.headers = auth;
    } else {
        init = {headers: auth};
    }

    return await fetch(input, init);
};

/**
 *
 * @param appName application name that wants the token
 * @param expiryTime when the token will expire
 * @return Returns a tuple of (success, errorMsg), where errorMsg is set if request failed
 */
export const genToken = async (appName: string, expiryTime: string) => {
    const endpoint = `${tokenURL}?app_name=${appName}`;
    return await authFetch(endpoint, {method: "POST"});
};

export const deleteToken = async (token: string) => {
    const endpoint = `${tokenURL}/${token}`;
    return await authFetch(endpoint, {method: "DELETE"});
};

export type FetchedToken = {
    token: string
    name: string
    exp: number
}

export const getTokens = async () => {
    const endpoint = tokenURL;

    const stream = await authFetch(endpoint, {method: "GET"});
    return await stream.json();
};

export const getSparqlQuery = async (queryString: string) => {
    const endpoint = sparqlURL;

    const sparqlHeader = new Headers();
    sparqlHeader.append("Content-Type", "application/sparql-query");

    const stream = await authFetch(endpoint, {method: "POST",
        headers: sparqlHeader,
        body: queryString});
    return await stream.json();
};

export const getEntity = async (entityId: string) => {
    const endpoint = `${entityURL}/${entityId}`;

    const entityHeader = new Headers();
    entityHeader.append("Content-Type", "application/json");

    const stream = await authFetch(endpoint, {method: "GET",
        headers: entityHeader,
    });
    return await stream.json();
}

export const getTimeseries = async (entityId: string, startTime: number, endTime: number) => {
    const endpoint = `${timeseriesURL}/${entityId}?start_time=${startTime}&end_time=${endTime}`;

    const entityHeader = new Headers();
    entityHeader.append("Content-Type", "application/json");

    const stream = await authFetch(endpoint, {method: "GET",
        headers: entityHeader,
    });
    return await stream.json();
}