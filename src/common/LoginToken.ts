import jwt_decode from 'jwt-decode';

export type LoginToken = {
    user_id: string
    exp: number
    app_id: string
}

export const getRawStoredToken = (): string | null => {
    return window.sessionStorage.getItem('token');
};

export const getStoredToken = (): LoginToken | null => {
    const tokenStr = getRawStoredToken();
    if (tokenStr) {
        return decodeToken(tokenStr);
    }
    return null;
};

export const decodeToken = (token: string): LoginToken => {
    return jwt_decode(token);
};

export const storeToken = (token: string) => {
    window.sessionStorage.setItem('token', token);
};

export const clearToken = () => {
    window.sessionStorage.removeItem("token");
};
