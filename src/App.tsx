import * as React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {MainLayout} from "layouts/MainLayout";
import {LoginHandle, LogoutHandle} from "./common/LoginHandler";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.1.0";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import {ConfirmRegistration} from "./pages/ConfirmRegistration";
import {PrivateRoute} from "./common/PrivateRoute";

class AppView extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <PrivateRoute exact path="/">
                        <Redirect to="/user-profile"/>
                    </PrivateRoute>

                    <Route path="/logged-in-success" render={() => <LoginHandle/>}/>
                    <Route exact path="/register" render={() => <ConfirmRegistration/>}/>

                    <PrivateRoute exact path="/logout" render={() => <LogoutHandle/>}/>

                    <Route path="/" render={props => <MainLayout {...props} />}/>
                </Switch>
            </BrowserRouter>
        );
    }
}

export const App = AppView;
