declare module "*.png" {
    const value: any;
    export = value;
}

declare module "*.svg" {
    const value: any
    export = value;
}

declare module "react-notification-alert" {
    declare var NotificationAlert: ClassComponent;
    export default NotificationAlert;
}

declare module "react-ui-tree" {
    import {Component} from "react";

    export interface TreeNode {
        module: string
        collapsed: boolean
        clickable: boolean
        parent?: TreeNode
        children?: Array<TreeNode>
        root: boolean
    }

    interface UITreeProps {
        tree: TreeNode
        paddingLeft: number
        draggable: boolean
        renderNode: Function
        onChange: Function
    }
    export default class Tree extends Component<UITreeProps> {}
}
