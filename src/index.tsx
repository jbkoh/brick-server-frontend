import React from "react";
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {App} from "./App";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import * as myReducers from "common/reducers";
import {getStoredToken} from "./common/LoginToken";
import {AuthFields} from "common/reducers";

export const AuthInitialState: AuthFields = {
    userId: "",
    isLoggedIn: false,
    isApproved: false,
    isAdmin: false,
};

let token = getStoredToken();
const authState: AuthFields = Object.assign(AuthInitialState, {});
if (token) {
    authState.userId = token.user_id;
    authState.isLoggedIn = true;
}
const initialState = {auth: authState};
const reducers = combineReducers(myReducers);
// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, initialState, composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById("root"));
