import * as React from 'react';
import {useEffect, useState} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import * as appApi from "common/api/app-api";
import {AppCardGroup, AppCardProps, AppData} from "./App.d";
import {useDispatch} from "react-redux";
import {fetchActivatedApps} from "../../common/actions";

const AppCard = (props: AppCardProps) => {
    const [description, setDescription] = useState("");
    const [modal, setModal] = useState(false);
    const [permissions, setPermissions] = useState("");

    const activate = () => {
        appApi.activateApp(props.appName).then(e => {
            if (props.reload) {
                props.reload();
            }
            toggle();
        }).catch(err => {
            console.error(err);
        });
    };

    const toggle = () => {
        setModal(!modal);
    };

    useEffect(() => {
        appApi.getAppData(props.appName).then((info: AppData) => {
            setDescription(info.description);
            if (info.permission_templates) {
                setPermissions(info.permission_templates!.all_points.permission_type);
            }
        });
    }, [props.appName]);

    return (
        <div className="card-wrapper">
            <Modal isOpen={modal} toggle={toggle} centered={true}>
                <ModalHeader toggle={toggle}>Activate App</ModalHeader>
                <ModalBody>
                    Are you sure you want to activate : <b>{props.appName}</b>?
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={activate}>Activate</Button>
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
            <Card>
                <CardHeader>
                    <CardTitle tag="h5">{props.appName}</CardTitle>
                </CardHeader>
                <CardBody>
                    {description}
                </CardBody>
                <CardFooter>
                    <div className="app-card--footer">
                        <div className="permissions">
                            {permissions ? permissions : "No permissions"}
                        </div>
                        <Button
                            onClick={toggle}
                            disabled={props.activated}
                            color="primary">
                            {props.activated ? "Activated" : "Activate"}
                        </Button>
                    </div>
                </CardFooter>
            </Card>
        </div>
    );
};

AppCard.defaultProps = {
    activated: false
};

export const ActivateApps = () => {
    const [activatedApps, setActivatedApps] = useState([] as string[]);
    const [approvedApps, setApprovedApps] = useState([] as string[]);
    const dispatch = useDispatch();

    const reload = () => {
        appApi.getApprovedApps().then(apps => {
            setApprovedApps(apps);
        });

        appApi.getActivatedApps().then(apps => {
            setActivatedApps(apps);
        });

        // Extra request to get activated apps, but it doesn't really matter
        dispatch(fetchActivatedApps());
    };

    useEffect(() => {
        reload();
    }, []);

    const cards = approvedApps
        .map((app, idx) =>
            <AppCard key={`AppCard ${idx}`}
                     appName={app}
                     reload={reload}
                     activated={activatedApps.includes(app)}/>);

    return (<AppCardGroup cards={cards} defaultMsg={"No apps activated"}/>);
};
