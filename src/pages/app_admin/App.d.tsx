import * as React from 'react';
import {Card, CardHeader, CardTitle, Container} from "reactstrap";

export type AllPoints = {
    common_variables: string[]
    permission_type: string
    target_variables: string[]
    queries: any
}

export type PermissionTemplate = {
    all_points: AllPoints
}

export type AppData = {
    description: string
    name: string
    permission_templates?: PermissionTemplate
}

export type AppCardProps = {
    appName: string
    activated?: boolean
    approved?: boolean
    pending?: boolean
    reload?: () => void
}

type DefaultCardProps = {
    msg: string
}

export const DefaultCard = (props: DefaultCardProps) => {
    return (
        <>
            <div className="card-wrapper">
                <Card>
                    <CardHeader>
                        <CardTitle tag="h5">{props.msg}</CardTitle>
                    </CardHeader>
                </Card>
            </div>
        </>)
};

type AppCardGroupProps = {
    cards: JSX.Element[]
    defaultMsg: string
}
export const AppCardGroup = (props: AppCardGroupProps) => {
    return (
        <div className="content app-container">
            <Container>
                {props.cards.length > 0 ? props.cards :
                    <DefaultCard msg={props.defaultMsg}/>
                }
            </Container>
        </div>);
};
