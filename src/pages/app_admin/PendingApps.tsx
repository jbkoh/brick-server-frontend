import * as React from 'react';
import {useEffect, useState} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import * as appApi from "common/api/app-api";
import {AppCardGroup, AppCardProps, AppData} from "./App.d";
import NotificationAlert from "react-notification-alert";
import {fetchPendingApps} from "../../common/actions";
import {useDispatch} from "react-redux";

const notificationAlert: React.RefObject<any> = React.createRef();
const notify = (place: string) => {
    const type = "success";
    const options = {
        place: place,
        message: (
            <div>
                <div>
                    Approved app successfully.
                </div>
                <div>
                    The app can be viewed in Activate Apps!
                </div>
            </div>
        ),
        type: type,
        icon: "nc-icon nc-check-2",
        autoDismiss: 4
    };

    if (notificationAlert.current) {
        notificationAlert.current.notificationAlert(options);
    }
};

const AppCard = (props: AppCardProps) => {
    const [description, setDescription] = useState("");
    const [modal, setModal] = useState(false);
    const [permissions, setPermissions] = useState("");

    const approve = () => {
        appApi.approveApp(props.appName).then(e => {
            if (props.reload) {
                props.reload();
            }
            setModal(!modal);
            notify("tr");
        });
    };

    const toggle = () => setModal(!modal);

    useEffect(() => {
        appApi.getAppData(props.appName).then((info: AppData) => {
            setDescription(info.description);
            if (info.permission_templates) {
                setPermissions(info.permission_templates!.all_points.permission_type);
            }
        });
    }, [props.appName]);

    return (
        <div className="card-wrapper">
            <Modal isOpen={modal} toggle={toggle} centered={true}>
                <ModalHeader toggle={toggle}>Approve App</ModalHeader>
                <ModalBody>
                    Are you sure you want to approve : <b>{props.appName}</b>?
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={approve}>Approve</Button>
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
            <Card>
                <CardHeader>
                    <CardTitle tag="h5">{props.appName}</CardTitle>
                </CardHeader>
                <CardBody>
                    {description}
                </CardBody>
                <CardFooter>
                    <div className="app-card--footer">
                        <div className="permissions">
                            {permissions ? permissions : "No permissions"}
                        </div>
                        <Button
                            onClick={toggle}
                            color="primary">
                            Approve
                        </Button>
                    </div>
                </CardFooter>
            </Card>
        </div>
    );
};

export const PendingApps = () => {
    const [appList, setAppList] = useState([] as string[]);
    const dispatch = useDispatch();

    const reload = () => {
        appApi.getPendingApps().then(apps => {
            setAppList(apps);
        });

        // Extra request to update pending apps, but it doesn't really matter
        dispatch(fetchPendingApps());
    };

    useEffect(() => {
        reload();
    }, []);

    const cards = appList.map((appName, idx) =>
        <AppCard key={`AppCard ${idx}`} appName={appName} reload={reload}/>);

    return (
        <>
            <div className="pending-apps">
                <NotificationAlert ref={notificationAlert}/>
            </div>
            <AppCardGroup cards={cards} defaultMsg={"No apps to approve"}/>
        </>);
};
