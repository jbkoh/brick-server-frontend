import * as React from 'react';
import {useEffect, useState} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import * as appApi from "common/api/app-api";
import {AppCardGroup, AppCardProps, AppData} from "./App.d";
import {useDispatch} from "react-redux";
import {fetchPendingApps} from "../../common/actions";

// Note, stage and install mean the same thing
const AppCard = (props: AppCardProps) => {
    const [description, setDescription] = useState("");
    const [modal, setModal] = useState(false);
    const [error, setError] = useState("");
    const [permissions, setPermissions] = useState("");

    const stage = () => {
        appApi.stageApp(props.appName).then(e => {
            if (props.reload) {
                props.reload();
            }
            toggle();
        }).catch(err => {
            setError("Failed to install app. Please contact the system administrator.")
        });
    };

    const toggle = () => {
        setError("");
        setModal(!modal);
    };

    useEffect(() => {
        appApi.getAppData(props.appName).then((info: AppData) => {
            console.log(info);
            setDescription(info.description);
            if(info.permission_templates) {
                setPermissions(info.permission_templates!.all_points.permission_type);
            }
        });
    }, [props.appName]);

    return (
        <div className="card-wrapper">
            <Modal isOpen={modal} toggle={toggle} centered={true}>
                <ModalHeader toggle={toggle}>Install App</ModalHeader>
                <ModalBody>
                    Are you sure you want to install : <b>{props.appName}</b>?
                    {error ?
                        <div className="error">{error}</div>
                        : ""}
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={stage}>Install</Button>
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
            <Card>
                <CardHeader>
                    <CardTitle tag="h5">{props.appName}</CardTitle>
                </CardHeader>
                <CardBody>
                    {description}
                </CardBody>
                <CardFooter>
                    <div className="app-card--footer">
                        <div className="permissions">
                            {permissions ? permissions : "No permissions"}
                        </div>
                        <Button
                            disabled={props.activated || props.pending || props.approved}
                            onClick={toggle}
                            color="primary">
                            {props.activated ? "Activated" :
                                props.pending ? "Pending" :
                                    props.approved ? "Approved" : "Install"}
                        </Button>
                    </div>
                </CardFooter>
            </Card>
        </div>
    );
};

AppCard.defaultProps = {
    activated: false,
    pending: false
};

export const AvailableApps = () => {
    const [appList, setAppList] = useState([] as string[]);
    const [approvedList, setApprovedList] = useState([] as string[]);
    const [pendingList, setPendingList] = useState([] as string[]);
    const [activatedList, setActivatedList] = useState([] as string[]);
    const dispatch = useDispatch();

    const reload = () => {
        appApi.getAvailableApps().then(apps => {
            setAppList(apps);
        });

        appApi.getPendingApps().then(apps => {
            setPendingList(apps);
        });
        // Extra request to update pending apps, but it doesn't really matter
        dispatch(fetchPendingApps());

        appApi.getApprovedApps().then(apps => {
            setApprovedList(apps);
        });

        appApi.getActivatedApps().then(apps => {
            setActivatedList(apps);
        });
    };

    useEffect(() => {
        reload();
    }, []);

    const cards = appList.map((appName, idx) =>
        <AppCard key={`AppCard ${idx}`}
                 reload={reload}
                 activated={activatedList.includes(appName)}
                 pending={pendingList.includes(appName)}
                 approved={approvedList.includes(appName)}
                 appName={appName}/>);

    return (<AppCardGroup cards={cards} defaultMsg={"No available apps"}/>);
};
