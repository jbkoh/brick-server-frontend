import * as React from 'react';
import {Card, CardBody, CardHeader, CardTitle, Col, Container, Row} from "reactstrap";
import * as logo from "assets/img/logo.png";
import * as api from "common/api/api";

export const ConfirmRegistration = (props: any) => {
    return (
        <div>
            <div className="empty-panel">
                <Container>
                    <Row>
                        <Col md="12">
                            <Card className="login">
                                <CardHeader>
                                    <CardTitle tag="h5">Confirm Registration</CardTitle>

                                    <div className="logo-wrapper">
                                        <a href={api.frontendURL} className="logo-normal">
                                            <img src={logo} alt={"Brick Logo"}/>
                                            <p>Brick Server</p>
                                        </a>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <Container className="options">
                                        <Row>
                                            <Col>
                                                <a href={api.registerURL} className="nav-link btn btn-lg btn-success">
                                                    Confirm
                                                </a>
                                            </Col>

                                            <Col>
                                                <a href={api.frontendURL} className="nav-link btn btn-lg btn-danger">
                                                    Reject
                                                </a>
                                            </Col>
                                        </Row>
                                    </Container>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
};

