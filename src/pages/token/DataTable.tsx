import React from "react";
//@ts-ignore
import BootstrapTable from 'react-bootstrap-table-next';
//@ts-ignore
import filterFactory, {textFilter} from 'react-bootstrap-table2-filter';

import * as api from 'common/api/api';
import {redirectOnError, reloadTokens} from 'common/api/api';
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {AppToken, MyReducerState} from "../../common/reducers";
import {convertToAppToken} from "../../common/AppToken";


const columns = [{
    dataField: 'token',
    text: 'token',
    sort: true,
    filter: textFilter({placeholder: "Search tokens"})
}, {
    dataField: 'appName',
    text: 'app name',
    sort: true,
    filter: textFilter({placeholder: "Search app names"})
}, {
    dataField: 'expiration',
    text: 'expiration',
    sort: true,
    filter: textFilter({placeholder: "Search expiration"})
},
    {
        align: 'center',
        dataField: 'copy',
        headerStyle: {width: '50px'},
        text: '',
    },
    {
        align: 'center',
        dataField: 'delete',
        headerStyle: {width: '50px'},
        text: '',
    }
];

type PropsFromRedux = {
    data: AppToken[]
    reloadTokens: () => void
}

class DataTableView extends React.Component<PropsFromRedux> {
    async componentDidMount(): Promise<void> {
        try {
            await this.props.reloadTokens();
        } catch (err) {
            redirectOnError(err);
        }
    }

    withCopy = (arr: AppToken[]) => (arr.map(a => ({
        ...a,
        copy: <i role="button" className="nc-icon nc-single-copy-04"
                 onClick={(evt) => this.copy(a.token)}/>
    })));

    copy = (token: string) => {
        navigator.clipboard.writeText(token);
    };

    withDelete = (arr: AppToken[]) => (arr.map(a => ({
        ...a,
        delete: <i role="button" className="nc-icon nc-simple-remove"
                   onClick={(evt) => this.delete(a.token)}/>
    })));

    withIcons = (arr: AppToken[]) => {
        return this.withCopy(this.withDelete(arr));
    };

    delete = async (token: string) => {
        try {
            await api.deleteToken(token);
            await this.props.reloadTokens();
            console.log("%c Deleted message successfully", "{color: red}");
        } catch (err) {
            redirectOnError(err);
        }
    };

    render() {
        return (
            <BootstrapTable bootstrap4
                            keyField='id'
                            striped
                            data={this.withIcons(this.props.data)}
                            filter={filterFactory()}
                            columns={columns}/>
        );
    }
}

const mapState = (state: MyReducerState) => ({
    data: convertToAppToken(state.tokens)
});

const mapDispatch = (dispatch: Dispatch) => ({
    reloadTokens: reloadTokens(dispatch)
});

export const DataTable = connect(mapState, mapDispatch)(DataTableView);




