import * as React from 'react';
import {useState} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    CardTitle,
    Col,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row
} from "reactstrap";

import * as api from "common/api/api";
import {reloadTokens} from "common/api/api";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import {DataTable} from "./DataTable";
import {useDispatch} from "react-redux";


export const TokenManagement = () => {
    return (
        <div className="content token-management">
            <Row>
                <Col md="12">
                    <Card>
                        <CardHeader className="token">
                            <CardTitle tag="h5">Token Management</CardTitle>
                            <AddTokenButton/>
                        </CardHeader>
                        <CardBody>
                            <DataTable/>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    );
};

const AddTokenButton = () => {
    const [expiryTime, setExpiryTime] = useState("");
    const [appName, setAppName] = useState("");
    const [validSubmit, setValidSubmit] = useState(true);
    const [errMsg, setErrMsg] = useState(null);
    const [isOpen, setOpen] = useState(false);

    const dispatch = useDispatch();
    const reload = reloadTokens(dispatch);

    const toggle = () => {
        // Reset values
        setOpen(!isOpen);
        setValidSubmit(true);
        setAppName("");
        setErrMsg(null);
        setExpiryTime("");
    };

    const submit = async () => {
        // Reset to reshow error message just in case
        setValidSubmit(true);
        setErrMsg(null);

        try {
            // Leaving the raw call to show the error message inside the popup
            await api.genToken(appName, expiryTime);
            console.log("%c Added token!", "color: green");

            await reload();
            // Get tokens
            toggle();
        } catch (err) {
            setValidSubmit(false);
            setErrMsg(err.message);
            console.log("%c Failed to add token: %s!", "color: red", err.message);
        }
    };

    return (
        <>
            <Button color="danger" onClick={toggle} className="add-token">
                <i className="nc-icon nc-simple-add"/>
                <span>Add Token</span>
            </Button>
            <Modal isOpen={isOpen} toggle={toggle} centered>
                <ModalHeader toggle={toggle}>Add Token</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="app_name">Application Name</Label>
                            <Input value={appName} id="app_name"
                                   onChange={({target}) => setAppName(target.value)}/>
                            <FormFeedback tooltip>That name is already taken!</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label for="expiry_time">Expiry Time</Label>
                            <Input value={expiryTime} id="expiry_time"
                                   placeholder={"3600"}
                                   onChange={({target}) => setExpiryTime(target.value)}/>
                            <FormFeedback tooltip>That time is invalid</FormFeedback>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    {!validSubmit &&
                    <div className="form-error">
                        <div>Uh oh. Something went wrong.</div>
                        <div>{errMsg ? `Error: ${errMsg}` : "No error message given."}</div>
                    </div>}
                    <Button color="primary" onClick={submit}>Add</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    );
};



