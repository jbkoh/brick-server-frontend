import * as React from 'react';
import {RouteComponentProps} from "react-router";
import {Card, CardBody, CardFooter, CardHeader, CardTitle, Col, Container, Row} from "reactstrap";


export const NotLoggedIn = (props: RouteComponentProps) => {
    return (
        <div className="content">
            <Container>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h5">Not Logged In</CardTitle>
                            </CardHeader>
                            <CardBody>
                                Login to view content
                            </CardBody>
                            <CardFooter>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>);
};
