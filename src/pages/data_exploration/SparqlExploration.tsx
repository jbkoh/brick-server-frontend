import * as React from 'react';
import { Card, CardBody, CardFooter, CardHeader, CardTitle, Col, Container, Row } from "reactstrap";
import { SparqlPlayground } from '../../components/sparql/SparqlPlayground';

export class SparqlExploration extends React.Component {
    render() {
        return (
            <div className="content">
                <Container>
                    <Row>
                        <Col md="12">
                            <Card>
                                <CardHeader>
                                    <CardTitle tag="h5">SPARQL Playground</CardTitle>
                                    <p className="card-category">Data Explorer</p>
                                </CardHeader>
                                <CardBody>
                                    <SparqlPlayground selectable={false} />
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>);
    }
}
