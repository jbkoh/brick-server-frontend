import * as React from 'react';
import {Card, CardBody, CardHeader, CardTitle, Col, Container, Row} from "reactstrap";
import MetadataView from "components/metadata/MetadataView";


export class MetadataExplorer extends React.Component {
    render() {
        return (
            <div className="content">
                <Container>
                    <Row>
                        <Col md="12">
                            <Card>
                                <CardHeader>
                                    <CardTitle tag="h5">Metadata Explorer</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <MetadataView/>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>);
    }
}
