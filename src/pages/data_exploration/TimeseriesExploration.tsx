import * as React from 'react';
import { Card, CardBody, CardFooter, CardHeader, CardTitle, Col, Container, Row } from "reactstrap";
import TimeseriesView from "../../components/timeseries/TimeseriesView";

export class TimeseriesExploration extends React.Component {
    render() {
        return (
            <div className="content">
                <Container>
                    <Row>
                        <Col md="12">
                            <Card>
                                <CardHeader>
                                    <CardTitle tag="h5">Timeseries Explorer</CardTitle>
                                    <p className="card-category">Data Explorer</p>
                                </CardHeader>
                                <CardBody>
                                    <TimeseriesView />
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>);
    }
}
