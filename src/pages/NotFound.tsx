import * as React from 'react';
import {RouteComponentProps} from "react-router";
import {Card, CardBody, CardFooter, CardHeader, CardTitle, Col, Container, Row} from "reactstrap";


export const NotFound = (props: RouteComponentProps) => {
    return (
        <div className="content">
            <Container>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h5">Page Not Found</CardTitle>
                            </CardHeader>
                            <CardBody>
                                Uh oh. Not found.
                            </CardBody>
                            <CardFooter>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>);
};
